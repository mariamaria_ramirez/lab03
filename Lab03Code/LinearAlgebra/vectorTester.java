//Maria Maria Ramirez 2041788
package LinearAlgebra;

public class vectorTester {
    public static void main(String[] args){
        Vector3d vector1 = new Vector3d(3.0, 2.5, 1.1);
        Vector3d vector2 = new Vector3d(4.0, 6.5, 2.2);

        System.out.println("Magnitude vector1: " + vector1.magnitude());
        System.out.println("Dot vector1 & vector2: " + vector1.dotProduct(vector2));
        Vector3d vector3 = vector2.add(vector2);
        System.out.println("Vector3: " + vector3);
    }
}
