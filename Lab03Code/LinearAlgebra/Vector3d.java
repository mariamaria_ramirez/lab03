//Maria Maria Ramirez 2041788

package LinearAlgebra;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x; 
        this.y = y; 
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }
    
    public double magnitude(){
        double magnitude = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
        return magnitude;
    }

    public double dotProduct(Vector3d vector){
        double dot = (this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ());
        return dot;
    }

    public Vector3d add(Vector3d vector2){
        double newX = this.x + vector2.getX();
        double newY = this.y + vector2.getY();
        double newZ = this.z + vector2.getZ();

        Vector3d vector3 = new Vector3d(newX, newY, newZ);
        return vector3;
    }
}