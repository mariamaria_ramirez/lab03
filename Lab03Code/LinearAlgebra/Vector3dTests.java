//Maria Maria Ramirez 2041788

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


public class Vector3dTests {
    
    @Test
    void testValues(){
        Vector3d v = new Vector3d(1, 2, 3);

        assertEquals(1, v.getX());
        assertEquals(2, v.getY());
        assertEquals(3, v.getZ());
    }

    @Test
    void testMagnitude(){
        Vector3d v = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(-2, -5, 6);

        assertEquals(Math.sqrt(Math.pow(1, 2) + Math.pow(2, 2) + Math.pow(3, 2)), Math.sqrt(Math.pow(v.getX(), 2) + Math.pow(v.getY(), 2) + Math.pow(v.getZ(), 2)));
        //with negative numbers
        assertEquals(Math.sqrt(Math.pow(-2, 2) + Math.pow(-5, 2) + Math.pow(6, 2)), Math.sqrt(Math.pow(v2.getX(), 2) + Math.pow(v2.getY(), 2) + Math.pow(v2.getZ(), 2)));
    }

    @Test
    void testDotProduct(){
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(3, 2, 1);

        assertEquals(10, v1.dotProduct(v2));
    }

    @Test
    void testAdd(){
        Vector3d v1 = new Vector3d(-1, 2, -3);
        Vector3d v2 = new Vector3d(4, -5, 8);
        Vector3d v3 = v1.add(v2);

        
        double[] v3Arr = {v3.getX(),v3.getY(),v3.getZ()};
        double[] expected = {3, -3, 5};

        assertArrayEquals(expected, v3Arr);

    }
}
